const fs = require('fs');
const path = require('path');
const chalk = require('chalk');

const packageBuffer = fs.readFileSync(path.join(__dirname, 'package.json'));
const { version } = JSON.parse(packageBuffer.toString('utf8'));

const environmentPath = path.join(__dirname, 'src/environments');
const environments = fs.readdirSync(
  environmentPath
).filter((fileName: string) => fileName.match(/environment\.\w+\.ts/));

const versionRegex = /(version: ).+/;
const tab = '  ';

environments.forEach((env: string) => {
  let currentEnv = fs.readFileSync(
    path.join(environmentPath, env),
    { encoding: 'utf8' }
  );

  if (versionRegex.test(currentEnv)) {
    currentEnv = currentEnv.replace(versionRegex, (_: string, p1: string) => `${p1}'${version}'`);
  } else {
    currentEnv = currentEnv.replace(
      /(.)(\s)(};)/,
      (_: string, p1: string, p2: string, p3: string) =>
        `${p1 === ',' ? p1 : `${p1},`}${p2}${tab}version: ${version}\n${p3}`
    );
  }

  console.log(
    chalk.blue(`Updating version number in ${env} (${version})`)
  );

  fs.writeFileSync(
    path.join(environmentPath, env),
    currentEnv
  );
});
