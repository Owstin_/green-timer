import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  transform(value: number): any {
    const time = new Date(value);
    const minutes = time.getMinutes();
    const seconds = time.getSeconds();
    const milliseconds = Math.floor(time.getMilliseconds() / 10);

    if (minutes < 1) {
      return `${seconds}.${this.padNumber(milliseconds)}`;
    } else {
      return `${minutes}:${this.padNumber(seconds)}.${this.padNumber(milliseconds)}`;
    }
  }

  padNumber(number: number) {
    return `0${number}`.slice(-2);
  }

}
