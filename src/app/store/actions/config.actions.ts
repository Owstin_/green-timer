import { Action } from '@ngrx/store';
import { Config } from '@store/models/config.model';

export const LOAD_CONFIG = '[CONFIG] Load config';
export const LOAD_CONFIG_SUCCESS = '[CONFIG] Load config success';
export const NEW_SESSION = '[CONFIG] New session';
export const NEW_SESSION_SUCCESS = '[CONFIG] New session success';
export const RENAME_SPLITTER = '[CONFIG] Rename splitter';
export const RENAME_SESSION = '[CONFIG] Rename session';
export const RENAME_SESSION_SUCCESS = '[CONFIG] Rename session success';
export const SWITCH_SESSION = '[CONFIG] Switch session';
export const SWITCH_SESSION_SUCCESS = '[CONFIG] Switch session success';
export const ENTER_EDIT_MODE = '[CONFIG] Enter edit mode';
export const ENTER_EDIT_MODE_SUCCESS = '[CONFIG] Enter edit mode success';
export const EXIT_EDIT_MODE = '[CONFIG] Exit edit mode';
export const EXIT_EDIT_MODE_SUCCESS = '[CONFIG] Exit edit mode success';

export class LoadConfig implements Action {
  readonly type = LOAD_CONFIG;
}

export class LoadConfigSuccess implements Action {
  readonly type = LOAD_CONFIG_SUCCESS;

  constructor(public payload: Config) {}
}

export class NewSession implements Action {
  readonly type = NEW_SESSION;
}

export class NewSessionSuccess implements Action {
  readonly type = NEW_SESSION_SUCCESS;

  constructor(public payload: Config) {}
}

export class RenameSplitter implements Action {
  readonly type = RENAME_SPLITTER;

  constructor(public payload: string) {}
}

export class RenameSession implements Action {
  readonly type = RENAME_SESSION;

  constructor(public payload: string) {}
}

export class RenameSessionSuccess implements Action {
  readonly type = RENAME_SESSION_SUCCESS;

  constructor(public payload: Config) {}
}

export class SwitchSession implements Action {
  readonly type = SWITCH_SESSION;

  constructor(public payload: string) {}
}

export class SwitchSessionSuccess implements Action {
  readonly type = SWITCH_SESSION_SUCCESS;

  constructor(public payload: Config) {}
}

export class EnterEditMode implements Action {
  readonly type = ENTER_EDIT_MODE;
}

export class EnterEditModeSuccess implements Action {
  readonly type = ENTER_EDIT_MODE_SUCCESS;

  constructor(public payload: Config) {}
}

export class ExitEditMode implements Action {
  readonly type = EXIT_EDIT_MODE;
}

export class ExitEditModeSuccess implements Action {
  readonly type = EXIT_EDIT_MODE_SUCCESS;

  constructor(public payload: Config) {}
}

export type Actions
  = LoadConfig
  | LoadConfigSuccess
  | NewSession
  | NewSessionSuccess
  | RenameSplitter
  | RenameSession
  | RenameSessionSuccess
  | SwitchSession
  | SwitchSessionSuccess
  | EnterEditMode
  | EnterEditModeSuccess
  | ExitEditMode
  | ExitEditModeSuccess;
