import { Action } from '@ngrx/store';
import { Solve } from '@store/models/solve.model';
import { Config } from '@store/models/config.model';

export const LOAD_TIMES = '[SESSION] Load times';
export const LOAD_TIMES_SUCCESS = '[SESSION] Load times success';
export const NEW_SESSION = '[SESSION] New session';
export const NEW_SESSION_SUCCESS = '[SESSION] New session success';
export const SAVE_SESSION = '[SESSION] Save session';
export const SAVE_SESSION_SUCCESS = '[SESSION] Save session success';
export const ADD_TIME = '[SESSION] Add';
export const ADD_TIME_SUCCESS = '[SESSION] Add success';
export const REMOVE_TIME = '[SESSION] Remove';
export const REMOVE_TIME_SUCCESS = '[SESSION] Remove success';
export const RENAME_SESSION = '[SESSION] Rename session';
export const RENAME_SESSION_SUCCESS = '[SESSION] Rename session success';

export class LoadTimes implements Action {
  readonly type = LOAD_TIMES;

  constructor(public payload: Config) {}
}

export class LoadTimesSuccess implements Action {
  readonly type = LOAD_TIMES_SUCCESS;

  constructor(public payload: Solve[]) {}
}

export class NewSession implements Action {
  readonly type = NEW_SESSION;
}

export class NewSessionSuccess implements Action {
  readonly type = NEW_SESSION_SUCCESS;

  constructor(public payload: Solve[]) {}
}

export class SaveSession implements Action {
  readonly type = SAVE_SESSION;
}

export class SaveSessionSuccess implements Action {
  readonly type = SAVE_SESSION_SUCCESS;
}

export class AddTime implements Action {
  readonly type = ADD_TIME;

  constructor(public payload: number) {}
}

export class AddTimeSuccess implements Action {
  readonly type = ADD_TIME_SUCCESS;

  constructor(public payload: Solve[]) {}
}

export class RemoveTime implements Action {
  readonly type = REMOVE_TIME;

  constructor(public payload: number) {}
}

export class RemoveTimeSuccess implements Action {
  readonly type = REMOVE_TIME_SUCCESS;

  constructor(public payload: Solve[]) {}
}

export class RenameSession implements Action {
  readonly type = RENAME_SESSION;

  constructor(public payload: {
    oldSessionName: string,
    newSessionName: string
  }) {}
}

export class RenameSessionSuccess implements Action {
  readonly type = RENAME_SESSION_SUCCESS;

  constructor(public payload: Solve[]) {}
}

export type Actions
  = NewSession
  | NewSessionSuccess
  | AddTime
  | AddTimeSuccess
  | RemoveTime
  | RemoveTimeSuccess
  | LoadTimes
  | LoadTimesSuccess
  | SaveSession
  | SaveSessionSuccess
  | RenameSession
  | RenameSessionSuccess;
