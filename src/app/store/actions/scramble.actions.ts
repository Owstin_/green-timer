import { Action } from '@ngrx/store';
import { Scramble } from '@store/models/scramble.model';

export const NEW_SCRAMBLE = '[SCRAMBLE] New scramble';
export const NEW_SCRAMBLE_SUCCESS = '[SCRAMBLE] New scramble success';

export class NewScramble implements Action {
  readonly type = NEW_SCRAMBLE;
}

export class NewScrambleSuccess implements Action {
  readonly type = NEW_SCRAMBLE_SUCCESS;

  constructor(public payload: Scramble) {}
}

export type Actions
  = NewScramble
  | NewScrambleSuccess;
