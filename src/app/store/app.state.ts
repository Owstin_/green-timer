import { Solve } from '@store/models/solve.model';
import { Config } from '@store/models/config.model';
import { Scramble } from '@store/models/scramble.model';

export interface AppState {
  readonly session: Solve[];
  readonly config: Config;
  readonly scramble: Scramble;
}
