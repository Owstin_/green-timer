import { Scramble } from '@store/models/scramble.model';
import * as scrambleActions from '@store/actions/scramble.actions';

export function scrambleReducer(
  state: Scramble,
  action: scrambleActions.Actions
) {
  switch (action.type) {
    case scrambleActions.NEW_SCRAMBLE:
      return { ...state };

    case scrambleActions.NEW_SCRAMBLE_SUCCESS:
      return action.payload;

    default:
      return { ...state };
  }
}

export const getScramble = (state: Scramble) => state.scramble_string;
