import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';

import { AppState } from '@store/app.state';
import * as fromSession from '@store/reducers/session.reducer';
import * as fromConfig from '@store/reducers/config.reducer';
import * as fromScramble from '@store/reducers/scramble.reducer';
import { Config } from '@store/models/config.model';
import { Scramble } from '@store/models/scramble.model';

export const reducers: ActionReducerMap<AppState> = {
  session: fromSession.sessionReducer,
  config: fromConfig.configReducer,
  scramble: fromScramble.scrambleReducer,
};

export const configFeature = createFeatureSelector<Config>('config');
export const scrambleFeature = createFeatureSelector<Scramble>('scramble');

// COnfig selectors
export const selectCurrentSession = createSelector(
  configFeature,
  fromConfig.getCurrentSession
);

export const selectSessionList = createSelector(
  configFeature,
  fromConfig.getSessionList
);

export const selectIsEditing = createSelector(
  configFeature,
  fromConfig.getIsEditing
);

// Scramble selectors
export const selectScramble = createSelector(
  scrambleFeature,
  fromScramble.getScramble
);
