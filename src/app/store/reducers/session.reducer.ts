import { Solve } from '@store/models/solve.model';
import * as sessionActions from '@store/actions/session.actions';

export function sessionReducer(state: Solve[] = [], action: sessionActions.Actions) {
  switch (action.type) {
    case sessionActions.LOAD_TIMES:
      return state;

    case sessionActions.LOAD_TIMES_SUCCESS:
      return [...action.payload];

    case sessionActions.NEW_SESSION:
      return state;

    case sessionActions.NEW_SESSION_SUCCESS:
      return action.payload;

    case sessionActions.SAVE_SESSION:
      return state;

    case sessionActions.SAVE_SESSION_SUCCESS:
      return state;

    case sessionActions.ADD_TIME:
      return state;

    case sessionActions.ADD_TIME_SUCCESS:
      return [...action.payload];

    case sessionActions.REMOVE_TIME:
      return state;

    case sessionActions.REMOVE_TIME_SUCCESS:
      return [...action.payload];

    case sessionActions.RENAME_SESSION:
      return state;

    case sessionActions.RENAME_SESSION_SUCCESS:
      return [...action.payload];

    default:
      return state;
  }
}
