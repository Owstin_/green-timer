import { Config } from '@store/models/config.model';
import * as configActions from '@store/actions/config.actions';

export function configReducer(
  state: Config,
  action: configActions.Actions
) {
  switch (action.type) {
    case configActions.LOAD_CONFIG:
      return { ...state };

    case configActions.LOAD_CONFIG_SUCCESS:
      return action.payload;

    case configActions.NEW_SESSION:
      return { ...state };

    case configActions.NEW_SESSION_SUCCESS:
      return action.payload;

    case configActions.RENAME_SESSION:
      return { ...state };

    case configActions.RENAME_SESSION_SUCCESS:
      return action.payload;

    case configActions.SWITCH_SESSION:
      return { ...state };

    case configActions.SWITCH_SESSION_SUCCESS:
      return action.payload;

    case configActions.ENTER_EDIT_MODE:
      return state;

    case configActions.ENTER_EDIT_MODE_SUCCESS:
      return action.payload;

    case configActions.EXIT_EDIT_MODE:
      return state;

    case configActions.EXIT_EDIT_MODE_SUCCESS:
      return action.payload;

    default:
      return state;
  }
}

export const getCurrentSession = (state: Config) => state.currentSession;
export const getSessionList = (state: Config) => state.sessionList;
export const getNumberOfSessions = (state: Config) => state.numberOfSessions;
export const getIsEditing = (state: Config) => state.isEditing;
