export interface Config {
  currentSession: string;
  sessionList: string[];
  numberOfSessions: number;
  isEditing: boolean;
}
