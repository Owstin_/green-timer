export interface Solve {
  time: number;
  scramble: string;
}
