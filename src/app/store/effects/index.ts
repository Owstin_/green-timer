import { ConfigEffects } from '@store/effects/config.effects';
import { SessionEffects } from '@store/effects/session.effects';
import { ScrambleEffects } from '@store/effects/scramble.effects';

export const effects: any[] = [
  ConfigEffects,
  SessionEffects,
  ScrambleEffects
];
