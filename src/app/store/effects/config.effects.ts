import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, withLatestFrom, flatMap } from 'rxjs/operators';

import { AppState } from '@store/app.state';
import * as configActions from '@store/actions/config.actions';
import * as sessionActions from '@store/actions/session.actions';
import { ConfigService } from '@services/config.service';

@Injectable()
export class ConfigEffects {
  constructor(
    private actions$: Actions,
    private config: ConfigService,
    private store: Store<AppState>
  ) {}

  @Effect()
  loadConfig$ = this.actions$.pipe(
    ofType(configActions.LOAD_CONFIG),
    switchMap(() =>
      this.config.loadConfig().pipe(
        map(config => new configActions.LoadConfigSuccess(config))
      )
    )
  );

  @Effect()
  loadConfigSuccess$ = this.actions$.pipe(
    ofType(configActions.LOAD_CONFIG_SUCCESS),
    map((action: configActions.LoadConfigSuccess) =>
      new sessionActions.LoadTimes(action.payload)
    ),
  );

  @Effect()
  newSession$ = this.actions$.pipe(
    ofType(configActions.NEW_SESSION),
    withLatestFrom(this.store.select('config')),
    switchMap(([_, oldConfig]) =>
      this.config.newSession(oldConfig).pipe(
        map(config => new configActions.NewSessionSuccess(config))
      )
    )
  );

  @Effect()
  newSessionSuccess$ = this.actions$.pipe(
    ofType(configActions.NEW_SESSION_SUCCESS),
    map(() => new sessionActions.NewSession())
  );

  @Effect()
  renameSpliter$ = this.actions$.pipe(
    ofType(configActions.RENAME_SPLITTER),
    map((action: configActions.RenameSession) => action.payload),
    withLatestFrom(this.store.select('config')),
    flatMap(([payload, config]) => [
      new configActions.RenameSession(payload),
      new sessionActions.RenameSession({
        oldSessionName: config.currentSession,
        newSessionName: payload
      })
    ])
  );

  @Effect()
  renameSession$ = this.actions$.pipe(
    ofType(configActions.RENAME_SESSION),
    map((action: configActions.RenameSession) => action.payload),
    withLatestFrom(this.store.select('config')),
    switchMap(([payload, state]) =>
      this.config.renameSession(state, payload).pipe(
        map(config => new configActions.RenameSessionSuccess(config))
      )
    )
  );

  @Effect()
  renameSessionSuccess$ = this.actions$.pipe(
    ofType(configActions.RENAME_SESSION_SUCCESS),
    map(() => new configActions.ExitEditMode())
  );

  @Effect()
  switchSession$ = this.actions$.pipe(
    ofType(configActions.SWITCH_SESSION),
    map((action: configActions.SwitchSession) => action.payload),
    withLatestFrom(this.store.select('config')),
    switchMap(([session, config]) =>
      this.config.switchSession(config, session).pipe(
        map(newConfig => new configActions.SwitchSessionSuccess(newConfig))
      )
    )
  );

  @Effect()
  switchSessionSucces$ = this.actions$.pipe(
    ofType(configActions.SWITCH_SESSION_SUCCESS),
    map((action: configActions.SwitchSessionSuccess) =>
      new sessionActions.LoadTimes(action.payload)
    )
  );

  @Effect()
  enterEditMode$ = this.actions$.pipe(
    ofType(configActions.ENTER_EDIT_MODE),
    withLatestFrom(this.store.select('config')),
    switchMap(([_, oldConfig]) =>
      this.config.toggleEditMode(oldConfig).pipe(
        map(config => new configActions.EnterEditModeSuccess(config))
      )
    )
  );

  @Effect()
  exitEditMode$ = this.actions$.pipe(
    ofType(configActions.EXIT_EDIT_MODE),
    withLatestFrom(this.store.select('config')),
    switchMap(([_, oldConfig]) =>
      this.config.toggleEditMode(oldConfig).pipe(
        map(config => new configActions.ExitEditModeSuccess(config))
      )
    )
  );
}
