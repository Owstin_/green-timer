import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { zip, of } from 'rxjs';
import { switchMap, map, withLatestFrom, concatMap } from 'rxjs/operators';

import { AppState } from '@store/app.state';
import * as sessionActions from '@store/actions/session.actions';
import * as scrambleActions from '@store/actions/scramble.actions';
import * as fromRoot from '@store/reducers';
import { SessionService } from '@services/session.service';

@Injectable()
export class SessionEffects {
  constructor(
    private actions$: Actions,
    private session: SessionService,
    private store: Store<AppState>
  ) {}

  @Effect()
  loadTimes$ = this.actions$.pipe(
    ofType(sessionActions.LOAD_TIMES),
    map((action: sessionActions.LoadTimes) => action.payload),
    switchMap(payload =>
      this.session.loadSession(payload.currentSession).pipe(
        map(times => new sessionActions.LoadTimesSuccess(times || []))
      )
    )
  );

  @Effect()
  loadTimesSuccess$ = this.actions$.pipe(
    ofType(sessionActions.LOAD_TIMES_SUCCESS),
    map(() => new scrambleActions.NewScramble())
  );

  @Effect()
  newSession$ = this.actions$.pipe(
    ofType(sessionActions.NEW_SESSION),
    withLatestFrom(this.store.select('config')),
    switchMap(([_, config]) =>
      this.session.newSession(config.currentSession).pipe(
        map(session => new sessionActions.NewSessionSuccess(session))
      )
    )
  );

  @Effect()
  newSessionSuccess$ = this.actions$.pipe(
    ofType(sessionActions.NEW_SESSION_SUCCESS),
    map(() => new scrambleActions.NewScramble())
  );

  @Effect()
  addTime$ = this.actions$.pipe(
    ofType(sessionActions.ADD_TIME),
    concatMap((action: sessionActions.AddTime) =>
      zip(
        of(action.payload),
        this.store.pipe(select(fromRoot.selectScramble)),
        this.store.select('session'),
        this.store.pipe(select(fromRoot.selectCurrentSession))
      )
    ),
    switchMap(([time, scramble, state, currentSession]) =>
      this.session.addSolve(
        state,
        { time, scramble },
        currentSession
      ).pipe(
        map(times => new sessionActions.AddTimeSuccess(times))
      )
    )
  );

  @Effect()
  addTimeSuccess$ = this.actions$.pipe(
    ofType(sessionActions.ADD_TIME_SUCCESS),
    map(() => new scrambleActions.NewScramble())
  );

  @Effect()
  removeTime$ = this.actions$.pipe(
    ofType(sessionActions.REMOVE_TIME),
    concatMap((action: sessionActions.RemoveTime) =>
      zip(
        of(action.payload),
        this.store.select('session'),
        this.store.select('config')
      )
    ),
    switchMap(([payload, state, config]) =>
      this.session.removeSolve(state, payload, config.currentSession).pipe(
        map(times => new sessionActions.RemoveTimeSuccess(times))
      )
    )
  );

  @Effect()
  saveSession$ = this.actions$.pipe(
    ofType(sessionActions.SAVE_SESSION),
    concatMap(() =>
      zip(
        this.store.select('session'),
        this.store.select('config')
      )
    ),
    switchMap(([state, config]) =>
      this.session.saveSession(state, config.currentSession).pipe(
        map(() => new sessionActions.SaveSessionSuccess())
      )
    )
  );

  @Effect()
  renameSession$ = this.actions$.pipe(
    ofType(sessionActions.RENAME_SESSION),
    map((action: sessionActions.RenameSession) => action.payload),
    withLatestFrom(this.store.select('session')),
    switchMap(([payload, state]) =>
      this.session.renameSession(state, payload.oldSessionName, payload.newSessionName).pipe(
        map(times => new sessionActions.RenameSessionSuccess(times))
      )
    )
  );
}
