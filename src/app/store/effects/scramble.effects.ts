import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { ScrambleService } from '@services/scramble.service';
import * as scrambleActions from '@store/actions/scramble.actions';
import { switchMap, map } from 'rxjs/operators';

@Injectable()
export class ScrambleEffects {
  constructor(
    private actions$: Actions,
    private scrambler: ScrambleService
  ) {}

  @Effect()
  newScramble$ = this.actions$.pipe(
    ofType(scrambleActions.NEW_SCRAMBLE),
    switchMap(() =>
      this.scrambler.getScramble().pipe(
        map(scramble => new scrambleActions.NewScrambleSuccess(scramble))
      )
    )
  );
}
