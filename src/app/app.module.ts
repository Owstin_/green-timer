import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { environment } from '@env/environment';

// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormatTimePipe } from '@pipes/format-time.pipe';
import { SessionComponent } from '@components/session/session.component';
import { ConfigComponent } from '@components/config/config.component';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { effects } from '@store/effects';
import { reducers } from '@store/reducers';
import { ScrambleComponent } from './components/scramble/scramble.component';
import { TitleService } from '@services/title.service';

@NgModule({
  declarations: [
    AppComponent,
    FormatTimePipe,
    SessionComponent,
    ConfigComponent,
    ScrambleComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    })
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (service: TitleService) => () => service.appendVersionNumber(),
      deps: [TitleService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
