import { Component, HostListener, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '@store/app.state';
import * as sessionActions from '@store/actions/session.actions';
import * as configActions from '@store/actions/config.actions';
import { TimerService } from '@services/timer.service';

const enum Keys {
  SPACE = 32
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'green-timer';
  time: Observable<number>;
  scramble$: Observable<string>;

  constructor(
    private timer: TimerService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.store.dispatch(new configActions.LoadConfig());
    this.time = this.timer.time;
  }

  ngOnDestroy() {
    // this doesn't work.
    this.store.dispatch(new sessionActions.SaveSession());
  }

  @HostListener('window:keyup', ['$event'])
  startTimer(e: KeyboardEvent) {
    if (e.keyCode === Keys.SPACE) {
      if (!this.timer.isStopped) {
        this.timer.start();
      } else {
        this.timer.ready();
      }
    }
  }

  @HostListener('window:keydown', ['$event'])
  stopTimer(e: KeyboardEvent) {
    if (this.timer.isOn) {
      this.timer.stop();
    } else {
      if (e.keyCode === Keys.SPACE && this.timer.isReady) {
        this.timer.reset();
      }
    }
  }
}
