import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '@store/app.state';
import * as fromRoot from '@store/reducers';

@Component({
  selector: 'app-scramble',
  templateUrl: './scramble.component.html',
  styleUrls: ['./scramble.component.scss']
})
export class ScrambleComponent implements OnInit {
  scramble$: Observable<string>;

  constructor(
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.scramble$ = this.store.pipe(select(fromRoot.selectScramble));
  }

}
