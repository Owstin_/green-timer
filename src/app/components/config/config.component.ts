import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '@store/app.state';
import * as configActions from '@store/actions/config.actions';
import * as fromConfig from '@store/reducers';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit {
  edit = false;
  renameSessionForm: FormGroup = null;
  sessionList$: Observable<string[]>;
  currentSession$: Observable<string>;

  constructor(
    private store: Store<AppState>,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.currentSession$ = this.store.pipe(select(fromConfig.selectCurrentSession));
    this.sessionList$ = this.store.pipe(select(fromConfig.selectSessionList));
    this.sessionList$.subscribe(val => console.log(val));
  }

  createForm() {
    this.currentSession$.subscribe(currentSession => {
      this.renameSessionForm = this.fb.group({
        sessionName: [currentSession]
      });
    });
  }

  editSessionName() {
    this.edit = true;
    this.createForm();
    this.store.dispatch(new configActions.EnterEditMode());
  }

  saveSessionName() {
    this.store.dispatch(new configActions.RenameSplitter(this.renameSessionForm.value.sessionName));
    this.edit = false;
    this.renameSessionForm = null;
  }

  newSession() {
    this.store.dispatch(new configActions.NewSession());
  }

  switchSession(session: string) {
    this.store.dispatch(new configActions.SwitchSession(session));
  }

}
