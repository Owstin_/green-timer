import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Solve } from '@store/models/solve.model';
import { AppState } from '@store/app.state';
import * as sessionActions from '@store/actions/session.actions';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  sessionData$: Observable<Solve[]>;

  constructor(
    private store: Store<AppState>,
  ) {}

  ngOnInit() {
    this.sessionData$ = this.store.pipe(select('session'));
  }

  removeTime(index: number) {
    this.store.dispatch(new sessionActions.RemoveTime(index));
  }

}
