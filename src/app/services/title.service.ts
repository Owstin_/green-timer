import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class TitleService {

  constructor(
    private title: Title,
  ) { }

  appendVersionNumber() {
    const title = this.title.getTitle();
    this.title.setTitle(`${title} - ${environment.version}`);
  }
}
