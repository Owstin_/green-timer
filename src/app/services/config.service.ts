import { Injectable } from '@angular/core';
import { Store } from 'idb-keyval';
import { switchMap } from 'rxjs/operators';

import { IndexedDbService } from '@services/indexed-db.service';
import { Config } from '@store/models/config.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private readonly dbName = 'config-db';
  private readonly storeName = 'config-store';
  private readonly configKey = 'config';
  private readonly defaultConfig: Config = {
    currentSession: 'session_0',
    sessionList: ['session_0'],
    numberOfSessions: 1,
    isEditing: false
  };

  configStore: Store = null;

  constructor(private idb: IndexedDbService) {
    this.configStore = this.idb.createStore(this.dbName, this.storeName);
  }

  loadConfig() {
    return this.idb.listKeys(this.configStore).pipe(
      switchMap(keys => {
        if (keys.length < 1) {
          return this.idb.setItem<Config>(
            this.configKey,
            this.defaultConfig,
            this.configStore
          );
        } else {
          return this.idb.getItem<Config>(this.configKey, this.configStore);
        }
      })
    );
  }

  newSession(config: Config) {
    const currentSession = 'session_' + config.numberOfSessions;
    return this.idb.setItem<Config>(
      this.configKey,
      {
        currentSession,
        sessionList: [currentSession, ...config.sessionList],
        numberOfSessions: ++config.numberOfSessions,
        isEditing: config.isEditing
      },
      this.configStore
    );
  }

  renameSession(config: Config, newCurrentSession: string) {
    const sessionNameIndex = config.sessionList.findIndex(name => name === config.currentSession);
    config.currentSession = newCurrentSession;
    config.sessionList.splice(sessionNameIndex, 1, newCurrentSession);
    return this.idb.setItem<Config>(
      this.configKey,
      config,
      this.configStore
    );
  }

  switchSession(config: Config, session: string) {
    return this.idb.setItem<Config>(
      this.configKey,
      { ...config, currentSession: session },
      this.configStore
    );
  }

  toggleEditMode(config: Config) {
    return this.idb.setItem<Config>(
      this.configKey,
      { ...config, isEditing: !config.isEditing },
      this.configStore
    );
  }

}
