import { Injectable } from '@angular/core';
import Scrambow from 'scrambow';
import { of, Observable } from 'rxjs';
import { Scramble } from '@store/models/scramble.model';

@Injectable({
  providedIn: 'root'
})
export class ScrambleService {
  private scrambler = new Scrambow();

  constructor() { }

  getScramble(): Observable<Scramble> {
    return of(this.scrambler.get()[0]);
  }
}
