import { Injectable } from '@angular/core';
import { Store, set, get, del, keys } from 'idb-keyval';
import { Observable, from, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IndexedDbService {
  createStore(dbName: string, storeName: string) {
    return new Store(dbName, storeName);
  }

  setItem<T>(key: string, val: T, store: Store): Observable<T> {
    return from(set(key, val, store)).pipe(
      switchMap(() => of(val))
    );
  }

  getItem<T>(key: string, store: Store): Observable<T> {
    return from(get<T>(key, store));
  }

  delItem(key: string, store: Store): Observable<void> {
    return from(del(key, store));
  }

  listKeys(store: Store): Observable<IDBValidKey[]> {
    return from(keys(store));
  }
}
