import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  interval,
  Observable,
  Subscription,
  BehaviorSubject,
} from 'rxjs';

import { AppState } from '@store/app.state';
import * as sessionActions from '@store/actions/session.actions';
import * as fromRoot from '@store/reducers';
import { take } from 'rxjs/operators';

const enum TimerState {
  NEW = 0,
  START = 1,
  TIMING = 2,
  STOP = 3,
  RESET = 4
}

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private timerState: TimerState = TimerState.NEW;
  private offset: number;
  private timeout: Subscription;
  private interval$: Observable<number> = interval(10);
  private startTime: number;
  private endTime: number;
  private time$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private isEditing$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.isEditing$ = this.store.pipe(select(fromRoot.selectIsEditing));
  }

  get time() {
    return this.time$.asObservable();
  }

  get isOn() {
    return this.timerState === TimerState.TIMING;
  }

  get isStopped() {
    return this.timerState === TimerState.STOP;
  }

  get isReady() {
    return this.timerState === TimerState.NEW;
  }

  private update() {
    const time = this.time$.getValue() + this.delta();
    this.time$.next(time);
  }

  private delta(): number {
    const now = Date.now();

    const elapsedTime = now - this.offset;
    this.offset = now;

    return elapsedTime;
  }

  private calculateTime(): number {
    return this.endTime - this.startTime;
  }

  start() {
    this.isCurrentlyEditing().subscribe(isEditing => {
      if (!isEditing && this.timerState === TimerState.NEW) {
        this.timerState = TimerState.TIMING;
        this.startTime = Date.now();
        this.offset = Date.now();
        this.timeout = this.interval$.subscribe(() => {
          this.update();
        });
      }
    });
  }

  stop() {
    if (this.timerState === TimerState.TIMING) {
      this.endTime = Date.now();
      const time = this.calculateTime();

      this.timeout.unsubscribe();
      this.timerState = TimerState.STOP;

      this.time$.next(time);
      this.store.dispatch(new sessionActions.AddTime(time));
    }
  }

  reset() {
    this.isCurrentlyEditing().subscribe(isEditing => {
      if (!isEditing) {
        this.time$.next(0);
      }
    });
  }

  ready() {
    if (this.timerState === TimerState.STOP) {
      this.timerState = TimerState.NEW;
    }
  }

  private isCurrentlyEditing() {
    return this.isEditing$.pipe(take(1));
  }
}
