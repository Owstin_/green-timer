import { Injectable } from '@angular/core';
import { Store } from 'idb-keyval';
import { switchMap } from 'rxjs/operators';

import { IndexedDbService } from '@services/indexed-db.service';
import { Solve } from '@store/models/solve.model';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private readonly dbName = 'session-db';
  private readonly storeName = 'session-store';
  sessionStore: Store = null;

  constructor(
    private idb: IndexedDbService,
  ) {
    this.sessionStore = this.idb.createStore(this.dbName, this.storeName);
  }

  // keeping this code in case there is a need to separate out loading a fresh session
  // private async createStore() {
  //   this.sessionStore = this.idb.createStore(this.dbName, this.storeName);
  //   const storeKeys = await this.idb.listKeys(this.sessionStore);
  //   if (storeKeys.length < 1) {
  //     await this.idb.setItem<Solve[]>('session_0', [], this.sessionStore);
  //   }
  // }

  loadSession(sessionName: string) {
    return this.idb.listKeys(this.sessionStore).pipe(
      switchMap(keys => {
        if (keys.length < 1) {
          return this.idb.getItem<Solve[]>(
              sessionName,
              this.sessionStore
            );
        } else {
          return this.idb.getItem<Solve[]>(sessionName, this.sessionStore);
        }
      })
    );
  }

  newSession(sessionName: string) {
    return this.idb.setItem<Solve[]>(sessionName, [], this.sessionStore);
  }

  addSolve(solves: Solve[], solve: Solve, sessionName: string) {
    return this.idb.setItem<Solve[]>(sessionName, solves.concat(solve), this.sessionStore);
  }

  removeSolve(solves: Solve[], removeIndex: number, sessionName: string) {
    solves.splice(removeIndex, 1);
    return this.idb.setItem<Solve[]>(sessionName, solves, this.sessionStore);
  }

  saveSession(solves: Solve[], sessionName: string) {
    return this.idb.setItem<Solve[]>(sessionName, solves, this.sessionStore);
  }

  renameSession(solves: Solve[], oldSessionName: string, newSessionName: string) {
    return this.idb.delItem(oldSessionName, this.sessionStore).pipe(
      switchMap(
        () => this.idb.setItem<Solve[]>(newSessionName, solves, this.sessionStore)
      )
    );
  }

}
